package controllers

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm/clause"
	"math"
	"net/url"
	"sidat-amws/database"
	"sidat-amws/models"
	"sidat-amws/models/reqresp"
	"sidat-amws/util"
	"strconv"
	"strings"
)

// AllBentukKelembagaan BentukKelembagaan list
// @Summary BentukKelembagaan list
// @Description BentukKelembagaan list
// @Tags Master
// @Accept json
// @Produce json
// @param nama query string false "Name to search"
// @param sort query string false "Sort by"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /master/bentuk-kelembagaan [get]
func AllBentukKelembagaan(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	var records []models.BentukKelembagaan
	sortStr := c.Query("sort", "")
	nama := c.Query("nama", "")

	baseQ := database.DB.Preload(clause.Associations)
	if nama != "" {
		baseQ = baseQ.Where("UPPER(name) LIKE ?", "%"+ strings.ToUpper(nama) +"%")
	}

	if sortStr != "" {
		sortParsed, err := url.PathUnescape(sortStr)
		if err != nil {
			return err
		}
		var s []reqresp.SortRequest
		err = json.Unmarshal([]byte(sortParsed), &s)
		if err != nil {
			return err
		}
		if s[0].Desc {
			baseQ = baseQ.Order(s[0].Selector + " desc")
		} else {
			baseQ = baseQ.Order(s[0].Selector)
		}
	}

	baseQ.Offset(offset).Limit(limit).Find(&records)
	baseQ.Model(&models.BentukKelembagaan{}).Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       records,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

// CreateBentukKelembagaan Create a BentukKelembagaan
// @Summary Create a BentukKelembagaan
// @Description Create a BentukKelembagaan
// @Tags Master
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param data body models.BentukKelembagaan true "BentukKelembagaan data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /master/bentuk-kelembagaan [post]
func CreateBentukKelembagaan(c *fiber.Ctx) error {
	var record models.BentukKelembagaan

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	result := database.DB.Preload(clause.Associations).Create(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "bentuk_kelembagaan", "insert", nil, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success creating record",
		Data:    record,
	})
}

// GetBentukKelembagaan Get single BentukKelembagaan data
// @Summary Get single BentukKelembagaan data
// @Description Get single BentukKelembagaan data
// @Tags Master
// @Accept json
// @Produce json
// @param id path int true "BentukKelembagaan ID"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 404 {object} reqresp.ErrorResponse
// @Router /master/bentuk-kelembagaan/{id} [get]
func GetBentukKelembagaan(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.BentukKelembagaan{Id: uint(id)}

	result := database.DB.Preload(clause.Associations).Find(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success getting a record",
		Data:    record,
	})
}

// UpdateBentukKelembagaan Update a BentukKelembagaan
// @Summary Update a BentukKelembagaan
// @Description Update a BentukKelembagaan
// @Tags Master
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "BentukKelembagaan ID"
// @param data body models.BentukKelembagaan true "BentukKelembagaan data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /master/bentuk-kelembagaan/{id} [put]
func UpdateBentukKelembagaan(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.BentukKelembagaan{Id: uint(id)}

	var oldData models.BentukKelembagaan
	database.DB.Where("id = ?", id).First(&oldData)

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	result := database.DB.Model(&record).Preload(clause.Associations).Updates(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to update record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "bentuk_kelembagaan", "update", &oldData, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating record",
		Data:    record,
	})
}

// DeleteBentukKelembagaan Delete a BentukKelembagaan
// @Summary Delete a BentukKelembagaan
// @Description Delete a BentukKelembagaan
// @Tags Master
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "BentukKelembagaan ID"
// @Success 204 {object} reqresp.SuccessResponse
// @Error 500 {object} reqresp.ErrorResponse
// @Router /master/bentuk-kelembagaan/{id} [delete]
func DeleteBentukKelembagaan(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.BentukKelembagaan{Id: uint(id)}

	var oldData models.BentukKelembagaan
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Delete(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to delete record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "bentuk_kelembagaan", "delete", &oldData, nil)

	c.Status(fiber.StatusNoContent)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "BentukKelembagaan record deleted",
		Data:    record,
	})
}
