package models

import "time"

type ForgotPassword struct {
	Id        uint      `json:"id" gorm:"primaryKey"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	UserId    uint      `json:"user_id"`
	User      User      `json:"user" gorm:"foreignKey:UserId;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
	Code      string    `json:"code"`
	UsedAt    time.Time `json:"used_at"`
	ExpiredAt time.Time `json:"expired_at"`
}

func (f ForgotPassword) TableName() string {
	return "user_mgt.forgot_password"
}
