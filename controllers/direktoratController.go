package controllers

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm/clause"
	"math"
	"net/url"
	"sidat-amws/database"
	"sidat-amws/models"
	"sidat-amws/models/reqresp"
	"sidat-amws/util"
	"strconv"
	"strings"
)

// AllDirektorat Direktorat list
// @Summary Direktorat list
// @Description Direktorat list
// @Tags Master
// @Accept json
// @Produce json
// @param nama query string false "Name to search"
// @param sort query string false "Sort by"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /master/direktorat [get]
func AllDirektorat(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	var records []models.Direktorat

	sortStr := c.Query("sort", "")
	nama := c.Query("nama", "")

	baseQ := database.DB.Preload(clause.Associations)
	if nama != "" {
		baseQ = baseQ.Where("UPPER(name) LIKE ?", "%"+ strings.ToUpper(nama) +"%")
	}

	if sortStr != "" {
		sortParsed, err := url.PathUnescape(sortStr)
		if err != nil {
			return err
		}
		var s []reqresp.SortRequest
		err = json.Unmarshal([]byte(sortParsed), &s)
		if err != nil {
			return err
		}
		if s[0].Desc {
			baseQ = baseQ.Order(s[0].Selector + " desc")
		} else {
			baseQ = baseQ.Order(s[0].Selector)
		}
	}

	baseQ.Offset(offset).Limit(limit).Find(&records)

	baseQ.Model(&models.Direktorat{}).Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       records,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

// CreateDirektorat Create a direktorat
// @Summary Create a direktorat
// @Description Create a direktorat
// @Tags Master
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param data body models.Direktorat true "Direktorat data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /master/direktorat [post]
func CreateDirektorat(c *fiber.Ctx) error {
	var record models.Direktorat

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	result := database.DB.Create(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "direktorat", "insert", nil, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success creating record",
		Data:    record,
	})
}

// GetDirektorat Get single direktorat data
// @Summary Get single direktorat data
// @Description Get single direktorat data
// @Tags Master
// @Accept json
// @Produce json
// @param id path int true "Direktorat ID"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 404 {object} reqresp.ErrorResponse
// @Router /master/direktorat/{id} [get]
func GetDirektorat(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Direktorat{Id: uint(id)}

	result := database.DB.Find(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success getting a record",
		Data:    record,
	})
}

// UpdateDirektorat Update a direktorat
// @Summary Update a direktorat
// @Description Update a direktorat
// @Tags Master
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Direktorat ID"
// @param data body models.Direktorat true "Direktorat data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /master/direktorat/{id} [put]
func UpdateDirektorat(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Direktorat{Id: uint(id)}

	var oldData models.Direktorat
	database.DB.Where("id = ?", id).First(&oldData)

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	result := database.DB.Model(&record).Updates(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to update record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "direktorat", "update", &oldData, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating record",
		Data:    record,
	})
}

// DeleteDirektorat Delete a direktorat
// @Summary Delete a direktorat
// @Description Delete a direktorat
// @Tags Master
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Direktorat ID"
// @Success 204 {object} reqresp.SuccessResponse
// @Error 500 {object} reqresp.ErrorResponse
// @Router /master/direktorat/{id} [delete]
func DeleteDirektorat(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	if id <=1 {
		return c.Status(fiber.StatusForbidden).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "You can't delete this directorate ID",
		})
	}

	record := models.Direktorat{Id: uint(id)}

	var oldData models.Direktorat
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Delete(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to delete record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "direktorat", "delete", &oldData, nil)

	c.Status(fiber.StatusNoContent)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Direktorat record deleted",
		Data:    record,
	})
}
