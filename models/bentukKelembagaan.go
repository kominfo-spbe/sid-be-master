package models

import (
	"time"
)

type BentukKelembagaan struct {
	Id        uint           `json:"id" gorm:"primaryKey"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	Name      string         `json:"name" gorm:"unique"`
	Icon      string         `json:"icon"`
}

func (b BentukKelembagaan) TableName() string {
	return "master_ref.bentuk_kelembagaan"
}
