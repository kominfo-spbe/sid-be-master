package controllers

import (
	"encoding/json"
	"math"
	"net/url"
	"sidat-amws/database"
	"sidat-amws/models"
	"sidat-amws/models/reqresp"
	"sidat-amws/util"
	"strconv"
	"strings"

	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm/clause"
)

// AllAdvokasi Advokasi list
// @Summary Advokasi list
// @Description Advokasi list
// @Tags Advokasi
// @Accept json
// @Produce json
// @param q query string false "Search term"
// @param tahun query int false "Tahun advokasi"
// @param sort query string false "Sort by"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /advokasi [get]
func AllAdvokasi(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	var records []models.Advokasi

	searchTerm := c.Query("q", "")
	tahun, _ := strconv.Atoi(c.Query("tahun", "0"))
	sortStr := c.Query("sort", "")

	baseQ := database.DB.Preload(clause.Associations)
	if searchTerm != "" {
		baseQ = baseQ.Where("UPPER(perkara) LIKE ?", "%"+strings.ToUpper(searchTerm)+"%").Or("UPPER(kasus_hukum) LIKE ?", "%"+strings.ToUpper(searchTerm)+"%").Or("UPPER(materi) LIKE ?", "%"+strings.ToUpper(searchTerm)+"%")
	}

	if tahun > 0 {
		baseQ = baseQ.Where("EXTRACT(YEAR FROM tanggal) = ?", tahun)
	}

	if sortStr != "" {
		sortParsed, err := url.PathUnescape(sortStr)
		if err != nil {
			return err
		}
		var s []reqresp.SortRequest
		err = json.Unmarshal([]byte(sortParsed), &s)
		if err != nil {
			return err
		}
		if s[0].Desc {
			baseQ = baseQ.Order(s[0].Selector + " desc")
		} else {
			baseQ = baseQ.Order(s[0].Selector)
		}
	}

	baseQ.Offset(offset).Limit(limit).Find(&records)

	baseQ.Model(&models.Advokasi{}).Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       records,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

// CreateAdvokasi Create a advokasi
// @Summary Create a advokasi
// @Description Create a advokasi
// @Tags Advokasi
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param data body models.Advokasi true "Advokasi data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /advokasi [post]
func CreateAdvokasi(c *fiber.Ctx) error {
	var record models.Advokasi

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	result := database.DB.Preload(clause.Associations).Create(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "advokasi", "insert", nil, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success creating record",
		Data:    record,
	})
}

// GetAdvokasi Get single advokasi data
// @Summary Get single advokasi data
// @Description Get single advokasi data
// @Tags Advokasi
// @Accept json
// @Produce json
// @param id path int true "Advokasi ID"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 404 {object} reqresp.ErrorResponse
// @Router /advokasi/{id} [get]
func GetAdvokasi(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Advokasi{Id: uint(id)}

	result := database.DB.Preload(clause.Associations).Find(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success getting a record",
		Data:    record,
	})
}

// UpdateAdvokasi Update a advokasi
// @Summary Update a advokasi
// @Description Update a advokasi
// @Tags Advokasi
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Advokasi ID"
// @param data body models.Advokasi true "Advokasi data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /advokasi/{id} [put]
func UpdateAdvokasi(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Advokasi{Id: uint(id)}

	var oldData models.Advokasi
	database.DB.Where("id = ?", id).First(&oldData)

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	result := database.DB.Model(&record).Preload(clause.Associations).Updates(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to update record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "advokasi", "update", &oldData, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating record",
		Data:    record,
	})
}

// DeleteAdvokasi Delete a Advokasi
// @Summary Delete a Advokasi
// @Description Delete a Advokasi
// @Tags Advokasi
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Advokasi ID"
// @Success 204 {object} reqresp.SuccessResponse
// @Error 500 {object} reqresp.ErrorResponse
// @Router /advokasi/{id} [delete]
func DeleteAdvokasi(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.Advokasi{Id: uint(id)}

	var oldData models.Advokasi
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Delete(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to delete record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "advokasi", "delete", &oldData, nil)

	c.Status(fiber.StatusNoContent)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Advokasi record deleted",
		Data:    record,
	})
}

// AdvokasiRegisteredYears Year advokasi list
// @Summary Year advokasi list
// @Description Year advokasi list
// @Tags Advokasi
// @Accept json
// @Produce json
// @Success 200 {object} reqresp.SuccessResponse
// @Router /advokasi/years [get]
func AdvokasiRegisteredYears(c *fiber.Ctx) error {
	var tahunList []string

	// Execute query
	result := database.DB.Model(&models.Advokasi{}).Distinct().Pluck("date_part('year', tanggal)", &tahunList)

	if result.Error != nil {
		// Handle the error
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to get year lists: " + result.Error.Error(),
		})
	}

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success getting year lists",
		Data:    tahunList,
	})
}
