package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"sidat-amws/database"
	"sidat-amws/models"
	"sidat-amws/models/reqresp"
	"sidat-amws/util"
	"strconv"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"gorm.io/gorm/clause"
)

type RegisterData struct {
	FirstName       string `json:"first_name"`
	LastName        string `json:"last_name"`
	Email           string `json:"email"`
	Password        string `json:"password"`
	PasswordConfirm string `json:"password_confirm"`
}

// Register Register new user
// @Summary Register new user
// @Description Register new user
// @Tags Auth
// @Accept json
// @Produce json
// @Param registerData body RegisterData true "Registration data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /auth/register [post]
func Register(c *fiber.Ctx) error {
	var data RegisterData
	err := c.BodyParser(&data)
	if err != nil {
		return err
	}

	if data.Password != data.PasswordConfirm {
		c.Status(400)
		return c.JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Password does not match",
		})
	}

	user := models.User{
		FirstName:    data.FirstName,
		LastName:     data.LastName,
		Email:        data.Email,
		Nip:          data.Email,
		Nik:          data.Email,
		RoleId:       1,
		DirektoratId: 1,
	}
	user.SetPassword(data.Password)

	result := database.DB.Create(&user)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create record: " + result.Error.Error(),
		})
	}

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Registrasi sukses",
		Data:    user,
	})
}

type userLogin struct {
	// you can use email, NIK or NIP to login
	Email              string
	Password           string
	GRecaptchaResponse string `json:"g-recaptcha-response" form:"g-recaptcha-response"`
}

// Login User login
// @Summary User login
// @Description User login
// @Tags Auth
// @Accept json
// @Produce json
// @Param loginData body userLogin true "User login data"
// @Success 200 {object} reqresp.TokenResponse
// @Error 400 {0bject} reqresp.ErrorResponse
// @Error 404 {0bject} reqresp.ErrorResponse
// @Router /auth/login [post]
func Login(c *fiber.Ctx) error {
	//viper.SetConfigFile(".env")
	//err := viper.ReadInConfig()
	//if err != nil {
	//	panic(fmt.Errorf("Fatal error config file: %w \n", err))
	//}

	var data userLogin
	if err := c.BodyParser(&data); err != nil {
		return err
	}

	// Recaptcha check
	fmt.Println(os.Getenv("RECAPTCHA_V3_SECRET_KEY"))
	recaptchaValid := c.Locals("recaptchaSuccess")
	if recaptchaValid != true {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Recaptcha gagal tervalidasi",
		})
	}

	var user models.User

	//check login by email
	database.DB.Preload(clause.Associations).Where("(Email=? OR nik=? OR nip=?) AND is_active=?", data.Email, data.Email, data.Email, true).First(&user)

	if user.Id == 0 {
		// check NIK
		// database.DB.Preload(clause.Associations).Where("nik=? AND is_active=?", data.Email, true).First(&user)
		// if user.Id == 0 {
		// check NIP
		// database.DB.Preload(clause.Associations).Where("nip=? AND is_active=?", data.Email, true).First(&user)
		// if user.Id == 0 {
		c.Status(404)
		return c.JSON(&reqresp.ErrorResponse{
			Message: "User not found",
			Status:  "error",
		})
		// }

		// }

	}

	err := user.ComparePassword(data.Password)
	if err != nil {
		c.Status(400)
		return c.JSON(&reqresp.ErrorResponse{
			Message: "Incorrect password",
			Status:  "error",
		})
	}

	var theRole models.Role
	database.DB.Preload(clause.Associations).Where("id = ?", user.RoleId).First(&theRole)

	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	tokenAgeHour, _ := strconv.Atoi(os.Getenv("TOKEN_AGE_HOUR"))

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["email"] = user.Email
	claims["user_id"] = user.Id
	claims["role"] = theRole
	claims["direktorat"] = user.Direktorat
	claims["kind"] = "access"
	claims["exp"] = time.Now().Add(time.Hour * time.Duration(tokenAgeHour)).Unix()

	// Generate encoded token and send it as reqresp.
	t, err := token.SignedString([]byte(os.Getenv("TOKEN_SECRET")))
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	// Create refresh token
	refreshToken := jwt.New(jwt.SigningMethodHS256)

	refreshTokenAgeHour, _ := strconv.Atoi(os.Getenv("REFRESH_TOKEN_AGE_HOUR"))

	// Set claims
	refreshClaims := refreshToken.Claims.(jwt.MapClaims)
	refreshClaims["email"] = user.Email
	refreshClaims["user_id"] = user.Id
	refreshClaims["role"] = theRole
	refreshClaims["direktorat"] = user.Direktorat
	refreshClaims["kind"] = "refresh"
	refreshClaims["exp"] = time.Now().Add(time.Hour * time.Duration(refreshTokenAgeHour)).Unix()

	// Generate encoded refresh token and send it as reqresp.
	rT, err := refreshToken.SignedString([]byte(os.Getenv("TOKEN_SECRET")))
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	cookie := fiber.Cookie{
		Name:  "jwt",
		Value: t,
		//Path:     "",
		//Domain:   "",
		//MaxAge:   0,
		Expires: time.Now().Add(time.Hour * time.Duration(tokenAgeHour)),
		//Secure:   false,
		HTTPOnly: true,
		//SameSite: "",
	}

	c.Cookie(&cookie)

	return c.JSON(&reqresp.TokenResponse{
		Message:      "success",
		AccessToken:  t,
		RefreshToken: rT,
	})

	//token, err := util.GenerateJwt(strconv.Itoa(int(user.Id)))
	//if err != nil {
	//	//return c.SendStatus(fiber.StatusInternalServerError)
	//	c.Status(fiber.StatusInternalServerError)
	//	return c.JSON(fiber.Map{
	//		"message": "failed to sign claims",
	//	})
	//}

	//cookie := fiber.Cookie{
	//	Name:     "jwt",
	//	Value:    token,
	//	//Path:     "",
	//	//Domain:   "",
	//	//MaxAge:   0,
	//	Expires:  time.Now().Add(time.Hour * 24),
	//	//Secure:   false,
	//	HTTPOnly: true,
	//	//SameSite: "",
	//}
	//
	//c.Cookie(&cookie)
	//
	//return c.JSON(fiber.Map{
	//	"message": "success",
	//})
}

type Claims struct {
	jwt.StandardClaims
}

// User Get logged in User info
// @Summary Get logged in User info
// @Description Get logged in User info
// @Tags Auth
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @Success 200 {object} models.User
// @Router /auth/user [get]
func User(c *fiber.Ctx) error {
	jwtUser := c.Locals("user").(*jwt.Token)
	jwtClaims := jwtUser.Claims.(jwt.MapClaims)

	//cookie := c.Cookies("jwt")
	//
	//id, _ := util.ParseJwt(cookie)

	var user models.User
	database.DB.Preload("Role").Preload("Direktorat").Where("id = ?", jwtClaims["user_id"]).First(&user)

	return c.JSON(user)
}

func Logout(c *fiber.Ctx) error {
	cookie := fiber.Cookie{
		Name:  "jwt",
		Value: "",
		//Path:     "",
		//Domain:   "",
		//MaxAge:   0,
		Expires: time.Now().Add(-time.Hour),
		//Secure:   false,
		HTTPOnly: true,
		//SameSite: "",
	}

	c.Cookie(&cookie)

	return c.JSON(fiber.Map{
		"message": "logout success",
	})
}

// UpdateInfo Update User Profile
// @Summary Update User Profile
// @Description Update User Profile
// @Tags Users
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @Param user body models.User true "User data"
// @Success 200 {object} models.User
// @Router /users/update-profile [post]
func UpdateInfo(c *fiber.Ctx) error {
	var data map[string]string
	if err := c.BodyParser(&data); err != nil {
		return err
	}

	//cookie := c.Cookies("jwt")
	//
	//id, _ := util.ParseJwt(cookie)
	//
	//userId, _ := strconv.Atoi(id)

	jwtUser := c.Locals("user").(*jwt.Token)
	jwtClaims := jwtUser.Claims.(jwt.MapClaims)

	userId := int(jwtClaims["user_id"].(float64))

	oldUser := models.User{
		Id: uint(userId),
	}
	var checkUser models.User

	database.DB.Where("id = ?", userId).Find(&oldUser)

	// check if nik is updated
	if (len(data["nik"]) > 0) && (oldUser.Nik != data["nik"]) {
		// check if nik already used
		database.DB.Preload(clause.Associations).Where("Nik=?", data["nik"]).First(&checkUser)
		if checkUser.Id != 0 {
			// already used
			c.Status(http.StatusBadRequest)
			return c.JSON(&reqresp.ErrorResponse{
				Message: "NIK already used",
				Status:  "error",
			})
		}
	}
	// check if nip is updated
	if (len(data["nip"]) > 0) && (oldUser.Nip != data["nip"]) {
		// check if nip already used
		database.DB.Preload(clause.Associations).Where("Nip=?", data["nip"]).First(&checkUser)
		if checkUser.Id != 0 {
			// already used
			c.Status(http.StatusBadRequest)
			return c.JSON(&reqresp.ErrorResponse{
				Message: "NIP already used",
				Status:  "error",
			})
		}
	}

	user := models.User{
		Id:        uint(userId),
		FirstName: data["first_name"],
		LastName:  data["last_name"],
		Email:     data["email"],
		Nip:       data["nip"],
		Nik:       data["nik"],
		NoTelp:    data["no_telp"],
	}

	database.DB.Model(&user).Updates(user)
	database.DB.Preload(clause.Associations).Where("id", uint(userId)).First(&user)

	return c.JSON(user)
}

type updatePassword struct {
	OldPassword     string
	Password        string
	PasswordConfirm string
}

// UpdatePassword Update User Password
// @Summary Update User Password
// @Description Update User Password
// @Tags Auth
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @Param newPassword body updatePassword true "New Password data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {0bject} reqresp.ErrorResponse
// @Router /auth/password [post]
func UpdatePassword(c *fiber.Ctx) error {
	var data updatePassword
	if err := c.BodyParser(&data); err != nil {
		return err
	}

	if data.Password != data.PasswordConfirm {
		c.Status(400)
		return c.JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "New Password and the confirmation does not match",
		})
	}

	//cookie := c.Cookies("jwt")
	//
	//id, _ := util.ParseJwt(cookie)
	//
	//userId, _ := strconv.Atoi(id)

	jwtUser := c.Locals("user").(*jwt.Token)
	jwtClaims := jwtUser.Claims.(jwt.MapClaims)

	userId := int(jwtClaims["user_id"].(float64))

	user := models.User{
		Id: uint(userId),
	}

	database.DB.Where("id = ?", userId).Find(&user)

	err := user.ComparePassword(data.OldPassword)
	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Old password is not correct",
		})
	}

	user.SetPassword(data.Password)
	database.DB.Model(&user).Updates(&user)
	database.DB.Preload(clause.Associations).Where("id", uint(userId)).First(&user)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating password",
		Data:    user,
	})
}

// RefreshToken Generate new access token
// @Summary Generate new access token
// @Description Generate new access token
// @Tags Token
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @Success 200 {object} reqresp.TokenResponse
// @Error 400 {0bject} reqresp.ErrorResponse
// @Router /token/refresh [post]
func RefreshToken(c *fiber.Ctx) error {
	jwtString := strings.Split(c.Get("Authorization", "Bearer invalid"), " ")[1]
	//jwtUser := c.Locals("user").(*jwt.Token)
	//viper.SetConfigFile(".env")
	//err := viper.ReadInConfig()
	//if err != nil {
	//	panic(fmt.Errorf("Fatal error config file: %w \n", err))
	//}

	var jwtClaims jwt.MapClaims
	_, err := jwt.ParseWithClaims(jwtString, &jwtClaims, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("TOKEN_SECRET")), nil
	})
	//fmt.Println(jwtUser)
	//jwtUser := jwtString.(*jwt.Token)
	//jwtClaims := jwtUser.Claims.(jwt.MapClaims)

	tokenKind := jwtClaims["kind"]

	if tokenKind != "refresh" {
		c.Status(400)
		return c.JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Wrong token type",
		})
	}

	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	tokenAgeHour, _ := strconv.Atoi(os.Getenv("TOKEN_AGE_HOUR"))

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["email"] = jwtClaims["email"]
	claims["user_id"] = jwtClaims["user_id"]
	claims["role"] = jwtClaims["role"]
	claims["direktorat"] = jwtClaims["direktorat"]
	claims["kind"] = "access"
	claims["exp"] = time.Now().Add(time.Hour * time.Duration(tokenAgeHour)).Unix()

	// Generate encoded token and send it as reqresp.
	t, err := token.SignedString([]byte(os.Getenv("TOKEN_SECRET")))
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	// Create refresh token
	refreshToken := jwt.New(jwt.SigningMethodHS256)

	refreshTokenAgeHour, _ := strconv.Atoi(os.Getenv("REFRESH_TOKEN_AGE_HOUR"))

	// Set claims
	refreshClaims := refreshToken.Claims.(jwt.MapClaims)
	refreshClaims["email"] = jwtClaims["email"]
	refreshClaims["user_id"] = jwtClaims["user_id"]
	refreshClaims["role"] = jwtClaims["role"]
	refreshClaims["direktorat"] = jwtClaims["direktorat"]
	refreshClaims["kind"] = "refresh"
	refreshClaims["exp"] = time.Now().Add(time.Hour * time.Duration(refreshTokenAgeHour)).Unix()

	// Generate encoded refresh token and send it as reqresp.
	rT, err := refreshToken.SignedString([]byte(os.Getenv("TOKEN_SECRET")))
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.JSON(&reqresp.TokenResponse{
		Message:      "success",
		AccessToken:  t,
		RefreshToken: rT,
	})
}

// InspectToken Inspect JWT Token
// @Summary Inspect JWT Token
// @Description Inspect JWT Token
// @Tags Token
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @Success 200 {object} jwt.Token
// @Router /token/inspect [post]
func InspectToken(c *fiber.Ctx) error {
	jwtUser := c.Locals("user").(*jwt.Token)

	return c.JSON(jwtUser)
}

// GenerateTokenFromKominfoToken Generate internal token based on Kominfo SSO token
// @Summary Generate internal token based on Kominfo SSO token
// @Description Generate internal token based on Kominfo SSO token
// @Tags Token
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @Success 200 {object} jwt.Token
// @Router /token/generate-token-from-sso [post]
func GenerateTokenFromKominfoToken(c *fiber.Ctx) error {
	jwtString := strings.Split(c.Get("Authorization", "Bearer invalid"), " ")[1]

	// call kominfo endpoint to verify and decode the token
	url := fmt.Sprintf("%s/auth/realms/SPBE/protocol/openid-connect/userinfo", os.Getenv("SSO_KOMINFO_URL"))
	method := "GET"

	payload := &bytes.Buffer{}
	writer := multipart.NewWriter(payload)
	err := writer.Close()
	if err != nil {
		fmt.Println(err)
		return err
	}

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		fmt.Println(err)
		return err
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", jwtString))

	req.Header.Set("Content-Type", writer.FormDataContentType())
	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return err
	}

	defer res.Body.Close()

	kominfoBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return err
	}
	fmt.Println(string(kominfoBody))
	if res.StatusCode != 200 {
		return c.Status(res.StatusCode).Send(kominfoBody)
	}
	// decode JSON string and get the email
	var unmarshalledBody map[string]interface{}
	err = json.Unmarshal(kominfoBody, &unmarshalledBody)
	if err != nil {
		return err
	}
	userEmail := unmarshalledBody["email"].(string)
	userNik := unmarshalledBody["nik"].(string)
	userNip := unmarshalledBody["nip"].(string)
	firstName := unmarshalledBody["given_name"].(string)
	lastName := unmarshalledBody["family_name"].(string)

	var user models.User

	//check login by email
	database.DB.Preload(clause.Associations).Where("Email=? AND is_active=?", userEmail, true).First(&user)

	if user.Id == 0 {
		// check NIK
		database.DB.Preload(clause.Associations).Where("nik=? AND is_active=?", userNik, true).First(&user)
		if user.Id == 0 {
			// check NIP
			database.DB.Preload(clause.Associations).Where("nip=? AND is_active=?", userNip, true).First(&user)
			if user.Id == 0 {
				// create new user
				user = models.User{
					FirstName:    firstName,
					LastName:     lastName,
					Email:        userEmail,
					Nip:          userNip,
					Nik:          userNik,
					RoleId:       1,
					DirektoratId: 1,
					IsActive:     true,
				}
				user.SetPassword(util.RandString(8))
				result := database.DB.Create(&user)
				if result.Error != nil || result.RowsAffected <= 0 {
					return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
						Status:  "error",
						Message: "Failed to create SSO user: " + result.Error.Error(),
					})
				}

				//c.Status(404)
				//return c.JSON(&reqresp.ErrorResponse{
				//	Message: "User not found",
				//	Status:  "error",
				//})
			}

		}
	}

	var theRole models.Role
	database.DB.Preload(clause.Associations).Where("id = ?", user.RoleId).First(&theRole)

	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	tokenAgeHour, _ := strconv.Atoi(os.Getenv("TOKEN_AGE_HOUR"))

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["email"] = user.Email
	claims["user_id"] = user.Id
	claims["role"] = theRole
	claims["direktorat"] = user.Direktorat
	claims["kind"] = "access"
	claims["exp"] = time.Now().Add(time.Hour * time.Duration(tokenAgeHour)).Unix()

	// Generate encoded token and send it as reqresp.
	t, err := token.SignedString([]byte(os.Getenv("TOKEN_SECRET")))
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	// Create refresh token
	refreshToken := jwt.New(jwt.SigningMethodHS256)

	refreshTokenAgeHour, _ := strconv.Atoi(os.Getenv("REFRESH_TOKEN_AGE_HOUR"))

	// Set claims
	refreshClaims := refreshToken.Claims.(jwt.MapClaims)
	refreshClaims["email"] = user.Email
	refreshClaims["user_id"] = user.Id
	refreshClaims["role"] = theRole
	refreshClaims["direktorat"] = user.Direktorat
	refreshClaims["kind"] = "refresh"
	refreshClaims["exp"] = time.Now().Add(time.Hour * time.Duration(refreshTokenAgeHour)).Unix()

	// Generate encoded refresh token and send it as reqresp.
	rT, err := refreshToken.SignedString([]byte(os.Getenv("TOKEN_SECRET")))
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	cookie := fiber.Cookie{
		Name:  "jwt",
		Value: t,
		//Path:     "",
		//Domain:   "",
		//MaxAge:   0,
		Expires: time.Now().Add(time.Hour * time.Duration(tokenAgeHour)),
		//Secure:   false,
		HTTPOnly: true,
		//SameSite: "",
	}

	c.Cookie(&cookie)

	return c.JSON(&reqresp.TokenResponse{
		Message:      "success",
		AccessToken:  t,
		RefreshToken: rT,
	})
}

// GenerateTokenFromPolpumToken Generate internal token based on Polpum SSO token
// @Summary Generate internal token based on Polpum SSO token
// @Description Generate internal token based on Polpum SSO token
// @Tags Token
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @Success 200 {object} jwt.Token
// @Router /token/generate-token-from-polpum-sso [post]
func GenerateTokenFromPolpumToken(c *fiber.Ctx) error {
	jwtString := strings.Split(c.Get("Authorization", "Bearer invalid"), " ")[1]

	// call kominfo endpoint to verify and decode the token
	url := fmt.Sprintf("%s/auth/realms/mini-sso/protocol/openid-connect/userinfo", os.Getenv("SSO_POLPUM_URL"))
	method := "GET"

	payload := &bytes.Buffer{}
	writer := multipart.NewWriter(payload)
	err := writer.Close()
	if err != nil {
		fmt.Println(err)
		return err
	}

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		fmt.Println(err)
		return err
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", jwtString))

	req.Header.Set("Content-Type", writer.FormDataContentType())
	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return err
	}

	defer res.Body.Close()

	polpumBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return err
	}
	fmt.Println(string(polpumBody))
	if res.StatusCode != 200 {
		return c.Status(res.StatusCode).Send(polpumBody)
	}
	// decode JSON string and get the email
	var unmarshalledBody map[string]interface{}
	err = json.Unmarshal(polpumBody, &unmarshalledBody)
	if err != nil {
		return err
	}
	userEmail := unmarshalledBody["email"].(string)
	//userNik := unmarshalledBody["nik"].(string)
	userNip := unmarshalledBody["NIP"].(string)
	firstName := unmarshalledBody["given_name"].(string)
	lastName := unmarshalledBody["family_name"].(string)

	var user models.User

	//check login by email
	database.DB.Preload(clause.Associations).Where("Email=? AND is_active=?", userEmail, true).First(&user)

	if user.Id == 0 {
		// check NIP
		database.DB.Preload(clause.Associations).Where("nip=? AND is_active=?", userNip, true).First(&user)
		if user.Id == 0 {
			// create new user
			user = models.User{
				FirstName:    firstName,
				LastName:     lastName,
				Email:        userEmail,
				Nip:          userNip,
				RoleId:       1,
				DirektoratId: 1,
				IsActive:     true,
			}
			user.SetPassword(util.RandString(8))
			result := database.DB.Create(&user)
			if result.Error != nil || result.RowsAffected <= 0 {
				return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
					Status:  "error",
					Message: "Failed to create SSO user: " + result.Error.Error(),
				})
			}

			//c.Status(404)
			//return c.JSON(&reqresp.ErrorResponse{
			//	Message: "User not found",
			//	Status:  "error",
			//})
		}

	}

	var theRole models.Role
	database.DB.Preload(clause.Associations).Where("id = ?", user.RoleId).First(&theRole)

	// Create token
	token := jwt.New(jwt.SigningMethodHS256)

	tokenAgeHour, _ := strconv.Atoi(os.Getenv("TOKEN_AGE_HOUR"))

	// Set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["email"] = user.Email
	claims["user_id"] = user.Id
	claims["role"] = theRole
	claims["direktorat"] = user.Direktorat
	claims["kind"] = "access"
	claims["exp"] = time.Now().Add(time.Hour * time.Duration(tokenAgeHour)).Unix()

	// Generate encoded token and send it as reqresp.
	t, err := token.SignedString([]byte(os.Getenv("TOKEN_SECRET")))
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	// Create refresh token
	refreshToken := jwt.New(jwt.SigningMethodHS256)

	refreshTokenAgeHour, _ := strconv.Atoi(os.Getenv("REFRESH_TOKEN_AGE_HOUR"))

	// Set claims
	refreshClaims := refreshToken.Claims.(jwt.MapClaims)
	refreshClaims["email"] = user.Email
	refreshClaims["user_id"] = user.Id
	refreshClaims["role"] = theRole
	refreshClaims["direktorat"] = user.Direktorat
	refreshClaims["kind"] = "refresh"
	refreshClaims["exp"] = time.Now().Add(time.Hour * time.Duration(refreshTokenAgeHour)).Unix()

	// Generate encoded refresh token and send it as reqresp.
	rT, err := refreshToken.SignedString([]byte(os.Getenv("TOKEN_SECRET")))
	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	cookie := fiber.Cookie{
		Name:  "jwt",
		Value: t,
		//Path:     "",
		//Domain:   "",
		//MaxAge:   0,
		Expires: time.Now().Add(time.Hour * time.Duration(tokenAgeHour)),
		//Secure:   false,
		HTTPOnly: true,
		//SameSite: "",
	}

	c.Cookie(&cookie)

	return c.JSON(&reqresp.TokenResponse{
		Message:      "success",
		AccessToken:  t,
		RefreshToken: rT,
	})
}
