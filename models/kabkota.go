package models

import (
	"time"
)

type Kabkota struct {
	Id         uint           `json:"id" gorm:"primaryKey"`
	CreatedAt  time.Time      `json:"created_at"`
	UpdatedAt  time.Time      `json:"updated_at"`
	Code       string         `json:"code" gorm:"unique"`
	Name       string         `json:"name"`
	ProvinsiId uint           `json:"provinsi_id"`
	Provinsi   Provinsi       `json:"provinsi" gorm:"foreignKey:ProvinsiId;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}

func (k Kabkota) TableName() string {
	return "master_ref.kabkota"
}
