package models

import (
	"time"
)

type Dokumentasi struct {
	Id                 uint                  `json:"id" gorm:"primaryKey"`
	CreatedAt          time.Time             `json:"created_at"`
	UpdatedAt          time.Time             `json:"updated_at"`
	Tahun              int                   `json:"tahun"`
	JenisPerundanganId uint                  `json:"jenis_perundangan_id"`
	JenisPerundangan   JenisPerundangan      `json:"jenis_perundangan" gorm:"foreignKey:JenisPerundanganId;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	Judul              string                `json:"judul"`
	Nama               string                `json:"nama"`
	ProdukPath         string                `json:"produk_path"`
	Sinopsis           string                `json:"sinopsis"`
	TotalView          int                   `json:"total_view" gorm:"default:0"`
	TotalDownload      int                   `json:"total_download" gorm:"default:0"`
	Progress           []DokumentasiProgress `json:"progress"`
}

func (d Dokumentasi) TableName() string {
	return "bispro.dokumentasi"
}
