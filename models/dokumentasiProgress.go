package models

import (
	"time"

	"gorm.io/datatypes"
)

type DokumentasiProgress struct {
	Id            uint           `json:"id" gorm:"primaryKey"`
	CreatedAt     time.Time      `json:"created_at"`
	UpdatedAt     time.Time      `json:"updated_at"`
	DokumentasiId uint           `json:"dokumentasi_id"`
	Dokumentasi   Dokumentasi    `json:"dokumentasi" gorm:"foreignKey:DokumentasiId;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
	Status        string         `json:"status"`
	Tentang       string         `json:"tentang"`
	Url           string         `json:"url"`
	Path          string         `json:"path"`
	Tanggal       datatypes.Date `json:"tanggal"`
}

func (p DokumentasiProgress) TableName() string {
	return "bispro.dokumentasi_progress"
}
