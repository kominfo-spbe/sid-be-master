package controllers

import (
	"crypto/tls"
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"gopkg.in/gomail.v2"
	"gorm.io/gorm/clause"
	"math"
	"net/url"
	"os"
	"sidat-amws/database"
	"sidat-amws/models"
	"sidat-amws/models/reqresp"
	"sidat-amws/util"
	"strconv"
	"strings"
	"time"
)

// AllForgotPassword ForgotPassword list
// @Summary ForgotPassword list
// @Description ForgotPassword list
// @Tags Auth
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param user_id query int false "User ID"
// @param sort query string false "Sort by"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /forgot-password [get]
func AllForgotPassword(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	userId, _ := strconv.Atoi(c.Query("user_id", "0"))
	sortStr := c.Query("sort", "")

	var records []models.ForgotPassword
	baseQ := database.DB.Preload(clause.Associations).Offset(offset).Limit(limit)
	if userId > 0 {
		baseQ = baseQ.Where("user_id = ?", userId)
	}

	if sortStr != "" {
		sortParsed, err := url.PathUnescape(sortStr)
		if err != nil {
			return err
		}
		var s []reqresp.SortRequest
		err = json.Unmarshal([]byte(sortParsed), &s)
		if err != nil {
			return err
		}
		if s[0].Desc {
			baseQ = baseQ.Order(s[0].Selector + " desc")
		} else {
			baseQ = baseQ.Order(s[0].Selector)
		}
	}

	//execute query
	baseQ.Offset(offset).Limit(limit).Find(&records)

	baseQ.Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       records,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

// CreateForgotPassword Create a ForgotPassword
// @Summary Create a ForgotPassword
// @Description Create a ForgotPassword
// @Tags Auth
// @Accept json
// @Produce json
// @param email query string true "User email"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /forgot-password/generate-code [post]
func CreateForgotPassword(c *fiber.Ctx) error {
	email := c.Query("email")

	var user models.User

	result := database.DB.Preload(clause.Associations).Where("email = ?", email).Find(&user)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	var record models.ForgotPassword
	record.UserId = user.Id
	record.Code = util.RandString(16)
	record.ExpiredAt = time.Now().Add(time.Hour * 24) // hanya berlaku 24 jam

	result = database.DB.Create(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create record: " + result.Error.Error(),
		})
	}

	//send email to user
	mailTemplate:= models.Lookup{Code: "forgot_pass_template"}
	database.DB.Where("code = ?", mailTemplate.Code).First(&mailTemplate)
	forgotPassFEUrl := models.Lookup{Code: "forgot_pass_url"}
	database.DB.Where("code = ?", forgotPassFEUrl.Code).First(&forgotPassFEUrl)
	forgotPassSubject := models.Lookup{Code: "forgot_pass_subject"}
	database.DB.Where("code = ?", forgotPassSubject.Code).First(&forgotPassSubject)

	finalFEUrl := strings.Replace(forgotPassFEUrl.Name, "{code}", record.Code, -1)
	r := strings.NewReplacer("{nama}", user.FirstName + " " + user.LastName, "{link-reset}", finalFEUrl)
	finalMailTemplate := r.Replace(mailTemplate.Name)

	m := gomail.NewMessage()
	m.SetHeader("From", os.Getenv("FROM_ADDR"))
	m.SetHeader("To", user.Email)
	//m.SetAddressHeader("Cc", "dan@example.com", "Dan")
	m.SetHeader("Subject", forgotPassSubject.Name)
	m.SetBody("text/html", finalMailTemplate)
	//m.Attach("/home/Alex/lolcat.jpg")

	smtpPort, _ := strconv.Atoi(os.Getenv("SMTP_PORT"))

	d := gomail.NewDialer(os.Getenv("SMTP_SERVER"), smtpPort, os.Getenv("SMTP_USER"), os.Getenv("SMTP_PASSWORD"))
	d.TLSConfig= &tls.Config{InsecureSkipVerify: true}

	// Do send the email to user
	if err := d.DialAndSend(m); err != nil {
		// failed to send email
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: err.Error(),
		})
	}

	// send to log
	//util.SendToAudit(c, "forgot_password", "insert", nil, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success creating record",
		Data:    record,
	})
}

// ValidateForgotPassword Validate ForgotPassword Code
// @Summary Validate ForgotPassword Code
// @Description Validate ForgotPassword Code
// @Tags Auth
// @Accept json
// @Produce json
// @param code path string true "Forgot password code"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /forgot-password/validate-code/{code} [get]
func ValidateForgotPassword(c *fiber.Ctx) error {
	code := c.Params("code")

	forgotPassData := models.ForgotPassword{Code: code}
	result := database.DB.Preload(clause.Associations).Where("code = ?", forgotPassData.Code).Find(&forgotPassData)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Code not found",
		})
	}

	user := models.User{Id: forgotPassData.UserId}
	result = database.DB.Preload(clause.Associations).Where("id = ?", user.Id).Find(&user)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "User not found",
		})
	}

	if time.Now().After(forgotPassData.ExpiredAt) {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Code expired",
		})
	}

	if forgotPassData.UsedAt.Year() > 1 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Code already used",
		})
	}

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Code valid",
		Data:    forgotPassData,
	})
}


// DoChangeForgotPassword Do change forgotten password
// @Summary Do change forgotten password
// @Description Do change forgotten password
// @Tags Auth
// @Accept json
// @Produce json
// @param code path string true "Forgot password code"
// @Param newPassword body updatePassword true "New Password data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /forgot-password/do-change/{code} [post]
func DoChangeForgotPassword(c *fiber.Ctx) error {

	code := c.Params("code")

	forgotPassData := models.ForgotPassword{Code: code}
	result := database.DB.Preload(clause.Associations).Where("code = ?", forgotPassData.Code).Find(&forgotPassData)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Code not found",
		})
	}

	user := models.User{Id: forgotPassData.UserId}
	result = database.DB.Preload(clause.Associations).Where("id = ?", user.Id).Find(&user)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "User not found",
		})
	}

	if time.Now().After(forgotPassData.ExpiredAt) {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Code expired",
		})
	}

	if forgotPassData.UsedAt.Year() > 1 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Code already used",
		})
	}

	var data updatePassword
	if err := c.BodyParser(&data); err != nil {
		return err
	}

	if data.Password != data.PasswordConfirm {
		c.Status(400)
		return c.JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Password does not match",
		})
	}

	user.SetPassword(data.Password)
	database.DB.Model(&user).Updates(&user)
	database.DB.Preload(clause.Associations).Where("id", user.Id).First(&user)

	// send to log
	//util.SendToAudit(c, "user", "update", "forgot password", &user)

	// update record forgotten pass
	forgotPassData.UsedAt = time.Now()
	database.DB.Updates(&forgotPassData)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating forgotten password",
		Data:    user,
	})
}

// DeleteForgotPassword Delete a ForgotPassword record
// @Summary Delete a ForgotPassword record
// @Description Delete a ForgotPassword record
// @Tags Auth
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "ForgotPassword ID"
// @Success 204 {object} reqresp.SuccessResponse
// @Error 500 {object} reqresp.ErrorResponse
// @Router /forgot-password/{id} [delete]
func DeleteForgotPassword(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	forgotPassword := models.ForgotPassword{Id: uint(id)}

	var oldData models.ForgotPassword
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Delete(&forgotPassword)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to delete record: " + result.Error.Error(),
		})
	}

	// send to log
	//util.SendToAudit(c, "forgot_password", "delete", &oldData, nil)

	c.Status(fiber.StatusNoContent)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "ForgotPassword record deleted",
		Data:    forgotPassword,
	})
}

