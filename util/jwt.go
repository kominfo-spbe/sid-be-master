package util

import (
	"github.com/dgrijalva/jwt-go"
	"time"
)

const SecretKey = "superSecret"

func GenerateJwt(issuer string) (string, error) {
	claims := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		//Audience:  "",
		ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
		//Id:        "",
		//IssuedAt:  0,
		Issuer:   issuer,
		//NotBefore: 0,
		//Subject:   "",
	})

	return claims.SignedString([]byte(SecretKey))
}

func ParseJwt(cookie string) (string, error) {
	token, err := jwt.ParseWithClaims(cookie, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(SecretKey), nil
	})

	if err != nil || !token.Valid {
		return "", err
	}

	claims := token.Claims.(*jwt.StandardClaims)

	return claims.Issuer, nil
}