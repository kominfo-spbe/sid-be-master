package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm/clause"
	"math"
	"os"
	"sidat-amws/database"
	"sidat-amws/models"
	"sidat-amws/models/reqresp"
	"sidat-amws/util"
	"strconv"
	"strings"
)

// AllDokumentasiProgress DokumentasiProgress list
// @Summary DokumentasiProgress list
// @Description DokumentasiProgress list
/// @Tags Dokumentasi
// @Accept json
// @Produce json
// @param dokumentasi_id query int true "Dokumentasi ID"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /dokumentasi-progress [get]
func AllDokumentasiProgress(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	DokumentasiId, _ := strconv.Atoi(c.Query("dokumentasi_id", "0"))

	var records []models.DokumentasiProgress

	database.DB.Preload(clause.Associations).Where("dokumentasi_id = ?", DokumentasiId).Offset(offset).Limit(limit).Find(&records)

	database.DB.Where("dokumentasi_id = ?", DokumentasiId).Model(&models.DokumentasiProgress{}).Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       records,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

// CreateDokumentasiProgress Create a DokumentasiProgress
// @Summary Create a DokumentasiProgress
// @Description Create a DokumentasiProgress
/// @Tags Dokumentasi
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param data body models.DokumentasiProgress true "DokumentasiProgress data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /dokumentasi-progress [post]
func CreateDokumentasiProgress(c *fiber.Ctx) error {
	var record models.DokumentasiProgress

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	result := database.DB.Preload(clause.Associations).Create(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "dokumentasi_progress", "insert", nil, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success creating record",
		Data:    record,
	})
}

// GetDokumentasiProgress Get single DokumentasiProgress data
// @Summary Get single DokumentasiProgress data
// @Description Get single DokumentasiProgress data
/// @Tags Dokumentasi
// @Accept json
// @Produce json
// @param id path int true "DokumentasiProgress ID"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 404 {object} reqresp.ErrorResponse
// @Router /dokumentasi-progress/{id} [get]
func GetDokumentasiProgress(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.DokumentasiProgress{Id: uint(id)}

	result := database.DB.Preload(clause.Associations).Find(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success getting a record",
		Data:    record,
	})
}

// UpdateDokumentasiProgress Update a DokumentasiProgress
// @Summary Update a DokumentasiProgress
// @Description Update a DokumentasiProgress
/// @Tags Dokumentasi
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "DokumentasiProgress ID"
// @param data body models.DokumentasiProgress true "DokumentasiProgress data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /dokumentasi-progress/{id} [put]
func UpdateDokumentasiProgress(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.DokumentasiProgress{Id: uint(id)}

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	var oldData models.DokumentasiProgress
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Model(&record).Preload(clause.Associations).Updates(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to update record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "dokumentasi_progress", "update", &oldData, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating record",
		Data:    record,
	})
}

// DeleteDokumentasiProgress Delete a DokumentasiProgress
// @Summary Delete a DokumentasiProgress
// @Description Delete a DokumentasiProgress
/// @Tags Dokumentasi
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "DokumentasiProgress ID"
// @Success 204 {object} reqresp.SuccessResponse
// @Error 500 {object} reqresp.ErrorResponse
// @Router /dokumentasi-progress/{id} [delete]
func DeleteDokumentasiProgress(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.DokumentasiProgress{Id: uint(id)}

	var oldData models.DokumentasiProgress
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Delete(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to delete record: " + result.Error.Error(),
		})
	}

	// delete uploaded file
	paths := strings.Split(record.Path, ";")
	for _, path := range paths {
		_ = os.Remove("./public/" + path)
	}
	//if err != nil {
	//	return c.Status(fiber.StatusInternalServerError).JSON(err)
	//}

	// send to log
	util.SendToAudit(c, "dokumentasi_progress", "delete", &oldData, nil)

	c.Status(fiber.StatusNoContent)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "DokumentasiProgress record deleted",
		Data:    record,
	})
}

