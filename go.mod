module sidat-amws

go 1.16

require (
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/arsmn/fiber-swagger/v2 v2.17.0
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-openapi/jsonreference v0.19.6 // indirect
	github.com/go-openapi/spec v0.20.4 // indirect
	github.com/go-openapi/swag v0.19.15 // indirect
	github.com/gofiber/fiber/v2 v2.18.0
	github.com/gofiber/jwt/v3 v3.0.2
	github.com/golang-jwt/jwt/v4 v4.0.0
	github.com/jackc/pgx/v4 v4.13.0 // indirect
	github.com/jansvabik/fiber-recaptcha v0.1.3 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/klauspost/compress v1.13.5 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/swaggo/files v0.0.0-20210815190702-a29dd2bc99b2 // indirect
	github.com/swaggo/swag v1.7.4
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
	golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2 // indirect
	golang.org/x/sys v0.0.0-20211116061358-0a5406a5449c // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.7 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	gorm.io/datatypes v1.0.2
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.15
)
