package routes

import (
	"os"
	"sidat-amws/controllers"

	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v3"
	recaptcha "github.com/jansvabik/fiber-recaptcha"
)

func Setup(app *fiber.App) {
	recaptcha.SecretKey = os.Getenv("RECAPTCHA_V3_SECRET_KEY")

	app.Post("/api/auth/register", controllers.Register)
	app.Post("/api/auth/login", recaptcha.Middleware, controllers.Login)

	app.Post("/api/token/refresh", controllers.RefreshToken)
	app.Post("/api/token/generate-token-from-sso", controllers.GenerateTokenFromKominfoToken)
	app.Post("/api/token/generate-token-from-polpum-sso", controllers.GenerateTokenFromPolpumToken)

	app.Get("/api/master/lookups", controllers.AllLookup)
	app.Get("/api/master/lookups/group-name", controllers.AllGroupName)
	app.Get("/api/master/lookups/search-group-name", controllers.SearchGroupName)
	app.Get("/api/master/lookups/by-group-name", controllers.AllNameByGroupName)
	app.Get("/api/master/lookups/:id", controllers.GetLookup)

	app.Get("/api/master/direktorat", controllers.AllDirektorat)
	app.Get("/api/master/direktorat/:id", controllers.GetDirektorat)

	app.Get("/api/master/subdirektorat", controllers.AllSubdirektorat)
	app.Get("/api/master/subdirektorat/:id", controllers.GetSubdirektorat)

	app.Get("/api/master/bentuk-kelembagaan", controllers.AllBentukKelembagaan)
	app.Get("/api/master/bentuk-kelembagaan/:id", controllers.GetBentukKelembagaan)

	app.Get("/api/master/jenis-perundangan", controllers.AllJenisPerundangan)
	app.Get("/api/master/jenis-perundangan/:id", controllers.GetJenisPerundangan)

	app.Get("/api/ref/provinsi", controllers.AllProvinsi)
	app.Get("/api/ref/provinsi/:id", controllers.GetProvinsi)

	app.Get("/api/ref/kabkota", controllers.AllKabkota)
	app.Get("/api/ref/kabkota/:id", controllers.GetKabkota)

	app.Get("/api/ref/kecamatan", controllers.AllKecamatan)
	app.Get("/api/ref/kecamatan/:id", controllers.GetKecamatan)

	app.Get("/api/advokasi", controllers.AllAdvokasi)
	app.Get("/api/advokasi/years", controllers.AdvokasiRegisteredYears)
	app.Get("/api/advokasi/:id", controllers.GetAdvokasi)
	app.Get("/api/advokasi-progress", controllers.AllAdvokasiProgress)
	app.Get("/api/advokasi-progress/:id", controllers.GetAdvokasiProgress)

	app.Get("/api/dokumentasi", controllers.AllDokumentasi)
	app.Get("/api/dokumentasi/stats", controllers.GetDokumentasiStats)
	app.Get("/api/dokumentasi/increase-view/:id", controllers.GetDokumentasiIncreaseView)
	app.Get("/api/dokumentasi/increase-download/:id", controllers.GetDokumentasiIncreaseDownload)
	app.Get("/api/dokumentasi/years", controllers.DocumentRegisteredYears)
	app.Get("/api/dokumentasi/:id", controllers.GetDokumentasi)

	app.Get("/api/produk-hukum", controllers.AllProdukHukum)
	app.Get("/api/produk-hukum/stats", controllers.GetProdukHukumStats)
	app.Get("/api/produk-hukum/years", controllers.ProhuRegisteredYears)
	app.Get("/api/produk-hukum/:id", controllers.GetProdukHukum)

	app.Get("/api/produk-hukum-progress", controllers.AllProdukHukumProgress)
	app.Get("/api/produk-hukum-progress/:id", controllers.GetProdukHukumProgress)

	app.Get("/api/dokumentasi-progress", controllers.AllDokumentasiProgress)
	app.Get("/api/dokumentasi-progress/:id", controllers.GetDokumentasiProgress)

	app.Get("/api/kelembagaan-provinsi", controllers.AllKelembagaanProvinsi)
	app.Get("/api/kelembagaan-provinsi/stats", controllers.GetStatsKelembagaanProvinsi)
	app.Get("/api/kelembagaan-provinsi/:id", controllers.GetKelembagaanProvinsi)

	app.Get("/api/kelembagaan-kota", controllers.AllKelembagaanKota)
	app.Get("/api/kelembagaan-kota/stats", controllers.GetStatsKelembagaanKota)
	app.Get("/api/kelembagaan-kota/:id", controllers.GetKelembagaanKota)

	app.Post("/api/forgot-password/generate-code", controllers.CreateForgotPassword)
	app.Get("/api/forgot-password/validate-code/:code", controllers.ValidateForgotPassword)
	app.Post("/api/forgot-password/do-change/:code", controllers.DoChangeForgotPassword)

	// must be logged-in
	//app.Use(middleware.IsAuthenticated)
	app.Use(jwtware.New(jwtware.Config{
		SigningKey: []byte(os.Getenv("TOKEN_SECRET")),
	}))
	app.Get("/api/auth/user", controllers.User)
	app.Post("/api/auth/logout", controllers.Logout)
	app.Post("/api/auth/password", controllers.UpdatePassword)

	app.Post("/api/token/inspect", controllers.InspectToken)

	app.Get("/api/file/info", controllers.FileInfo)
	app.Post("/api/file/upload", controllers.FileUploadSingle)
	app.Post("/api/file/multi-upload", controllers.FileUploadMulti)
	app.Delete("/api/file/delete", controllers.FileDelete)

	app.Get("/api/users", controllers.AllUsers)
	app.Post("/api/users", controllers.CreateUser)
	app.Post("/api/users/update-profile", controllers.UpdateInfo)
	app.Get("/api/users/by-email", controllers.GetUserByEmail)
	app.Put("/api/users/reset-password/:id", controllers.ResetPasswordUser)
	app.Get("/api/users/:id", controllers.GetUser)
	app.Put("/api/users/:id", controllers.UpdateUser)
	app.Delete("/api/users/:id", controllers.DeleteUser)

	app.Get("/api/roles", controllers.AllRoles)
	app.Post("/api/roles", controllers.CreateRole)
	app.Get("/api/roles/:id", controllers.GetRole)
	app.Put("/api/roles/:id", controllers.UpdateRole)
	app.Delete("/api/roles/:id", controllers.DeleteRole)

	app.Get("/api/permissions", controllers.AllPermissions)
	app.Post("/api/permissions", controllers.CreatePermission)
	app.Get("/api/permissions/check", controllers.CheckPermission)
	app.Get("/api/permissions/:id", controllers.GetPermission)
	app.Put("/api/permissions/:id", controllers.UpdatePermission)
	app.Delete("/api/permissions/:id", controllers.DeletePermission)

	app.Get("/api/audit-trail", controllers.AllAuditTrail)
	app.Get("/api/audit-trail/:id", controllers.GetAuditTrail)

	app.Post("/api/master/lookups", controllers.CreateLookup)
	app.Put("/api/master/lookups/:id", controllers.UpdateLookup)
	app.Delete("/api/master/lookups/:id", controllers.DeleteLookup)

	app.Post("/api/master/direktorat", controllers.CreateDirektorat)
	app.Put("/api/master/direktorat/:id", controllers.UpdateDirektorat)
	app.Delete("/api/master/direktorat/:id", controllers.DeleteDirektorat)

	app.Post("/api/master/subdirektorat", controllers.CreateSubdirektorat)
	app.Put("/api/master/subdirektorat/:id", controllers.UpdateSubdirektorat)
	app.Delete("/api/master/subdirektorat/:id", controllers.DeleteSubdirektorat)

	app.Post("/api/master/bentuk-kelembagaan", controllers.CreateBentukKelembagaan)
	app.Put("/api/master/bentuk-kelembagaan/:id", controllers.UpdateBentukKelembagaan)
	app.Delete("/api/master/bentuk-kelembagaan/:id", controllers.DeleteBentukKelembagaan)

	app.Post("/api/master/jenis-perundangan", controllers.CreateJenisPerundangan)
	app.Put("/api/master/jenis-perundangan/:id", controllers.UpdateJenisPerundangan)
	app.Delete("/api/master/jenis-perundangan/:id", controllers.DeleteJenisPerundangan)

	app.Post("/api/ref/provinsi", controllers.CreateProvinsi)
	app.Put("/api/ref/provinsi/:id", controllers.UpdateProvinsi)
	app.Delete("/api/ref/provinsi/:id", controllers.DeleteProvinsi)

	app.Post("/api/ref/kabkota", controllers.CreateKabkota)
	app.Put("/api/ref/kabkota/:id", controllers.UpdateKabkota)
	app.Delete("/api/ref/kabkota/:id", controllers.DeleteKabkota)

	app.Post("/api/ref/kecamatan", controllers.CreateKecamatan)
	app.Put("/api/ref/kecamatan/:id", controllers.UpdateKecamatan)
	app.Delete("/api/ref/kecamatan/:id", controllers.DeleteKecamatan)

	app.Post("/api/advokasi", controllers.CreateAdvokasi)
	app.Put("/api/advokasi/:id", controllers.UpdateAdvokasi)
	app.Delete("/api/advokasi/:id", controllers.DeleteAdvokasi)

	app.Post("/api/advokasi-progress", controllers.CreateAdvokasiProgress)
	app.Put("/api/advokasi-progress/:id", controllers.UpdateAdvokasiProgress)
	app.Delete("/api/advokasi-progress/:id", controllers.DeleteAdvokasiProgress)

	app.Post("/api/dokumentasi", controllers.CreateDokumentasi)
	app.Put("/api/dokumentasi/:id", controllers.UpdateDokumentasi)
	app.Delete("/api/dokumentasi/:id", controllers.DeleteDokumentasi)

	app.Post("/api/produk-hukum", controllers.CreateProdukHukum)
	app.Put("/api/produk-hukum/:id", controllers.UpdateProdukHukum)
	app.Delete("/api/produk-hukum/:id", controllers.DeleteProdukHukum)

	app.Post("/api/produk-hukum-progress", controllers.CreateProdukHukumProgress)
	app.Put("/api/produk-hukum-progress/:id", controllers.UpdateProdukHukumProgress)
	app.Delete("/api/produk-hukum-progress/:id", controllers.DeleteProdukHukumProgress)

	app.Post("/api/dokumentasi-progress", controllers.CreateDokumentasiProgress)
	app.Put("/api/dokumentasi-progress/:id", controllers.UpdateDokumentasiProgress)
	app.Delete("/api/dokumentasi-progress/:id", controllers.DeleteDokumentasiProgress)

	app.Post("/api/kelembagaan-provinsi", controllers.CreateKelembagaanProvinsi)
	app.Put("/api/kelembagaan-provinsi/:id", controllers.UpdateKelembagaanProvinsi)
	app.Delete("/api/kelembagaan-provinsi/:id", controllers.DeleteKelembagaanProvinsi)

	app.Post("/api/kelembagaan-kota", controllers.CreateKelembagaanKota)
	app.Put("/api/kelembagaan-kota/:id", controllers.UpdateKelembagaanKota)
	app.Delete("/api/kelembagaan-kota/:id", controllers.DeleteKelembagaanKota)

	app.Get("/api/notifikasi", controllers.AllNotifikasi)
	app.Post("/api/notifikasi", controllers.CreateNotifikasi)
	app.Get("/api/notifikasi/:id", controllers.GetNotifikasi)
	app.Put("/api/notifikasi/:id", controllers.UpdateNotifikasi)
	app.Delete("/api/notifikasi/:id", controllers.DeleteNotifikasi)

	app.Get("/api/forgot-password", controllers.AllForgotPassword)
	app.Delete("/api/forgot-password/:id", controllers.DeleteForgotPassword)
}
