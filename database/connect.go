package database

import (
	"fmt"
	"os"
	"sidat-amws/models"
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

var DB *gorm.DB

func Connect() {
	db, err := gorm.Open(postgres.Open(os.Getenv("DSN")), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			//TablePrefix:   "public.", // this is the schema
			SingularTable: true,
		},
	})

	if err != nil {
		panic(fmt.Errorf("Fatal error connect DB: %w \n", err))
	}

	fmt.Println("Db connect success")
	DB = db
	//DB.Exec(`set search_path='public'`)

	sqlDB, err := db.DB()

	if err == nil {
		// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
		sqlDB.SetMaxIdleConns(0)

		// SetMaxOpenConns sets the maximum number of open connections to the database.
		sqlDB.SetMaxOpenConns(100)

		// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
		sqlDB.SetConnMaxLifetime(time.Hour)
	}

	db.AutoMigrate(&models.User{}, &models.Role{}, &models.Permission{}, &models.Notifikasi{}, &models.ForgotPassword{}, &models.Provinsi{}, &models.Kabkota{}, &models.Kecamatan{}, &models.Lookup{}, &models.Direktorat{}, &models.Subdirektorat{}, &models.JenisPerundangan{}, &models.BentukKelembagaan{}, &models.Advokasi{}, &models.AdvokasiProgress{}, &models.KelembagaanKota{}, &models.KelembagaanProvinsi{}, &models.ProdukHukum{}, &models.ProdukHukumProgress{}, &models.Dokumentasi{}, &models.DokumentasiProgress{}, &models.AuditTrail{})
}
