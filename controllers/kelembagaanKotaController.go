package controllers

import (
	"encoding/json"
	"math"
	"net/url"
	"sidat-amws/database"
	"sidat-amws/models"
	"sidat-amws/models/reqresp"
	"sidat-amws/util"
	"strconv"
	"strings"

	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm/clause"
)

// AllKelembagaanKota KelembagaanKota list
// @Summary KelembagaanKota list
// @Description KelembagaanKota list
// @Tags Statistik Kelembagaan
// @Accept json
// @Produce json
// @param provinsi_id query int false "Provinsi ID"
// @param kabkota_id query int false "Kabkota ID"
// @param bentuk_kelembagaan_id query int false "Bentuk Kelembagaan ID"
// @param nomenklatur query string false "Nomenklatur"
// @param q query string false "Search query"
// @param sort query string false "Sort by"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /kelembagaan-kota [get]
func AllKelembagaanKota(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	kabkotaId, _ := strconv.Atoi(c.Query("kabkota_id", "0"))
	provinsiId, _ := strconv.Atoi(c.Query("provinsi_id", "0"))
	bentukKelembagaanId, _ := strconv.Atoi(c.Query("bentuk_kelembagaan_id", "0"))
	nomenklatur := c.Query("nomenklatur", "")
	q := c.Query("q", "")
	sortStr := c.Query("sort", "")

	var records []models.KelembagaanKota

	baseQ := database.DB.Preload(clause.Associations)
	if kabkotaId > 0 {
		baseQ = baseQ.Where("kabkota_id = ?", kabkotaId)
	}

	if provinsiId > 0 {
		baseQ = baseQ.Where("kabkota_id IN (select id from master_ref.kabkota WHERE provinsi_id = ?)", provinsiId)
	}

	if bentukKelembagaanId > 0 {
		baseQ = baseQ.Where("bentuk_kelembagaan_id = ?", bentukKelembagaanId)
	}

	if nomenklatur != "" {
		baseQ = baseQ.Where("UPPER(nomenklatur) LIKE ?", "%"+strings.ToUpper(nomenklatur)+"%")
	}

	// Feature search all columns
	if q != "" {
		// Do join kabkota for sorting
		baseQ = baseQ.Joins("LEFT JOIN kabkota ON kelembagaan_kota.kabkota_id = kabkota.id")
		// Do join bentuk kelembagaan for sorting
		baseQ = baseQ.Joins("LEFT JOIN bentuk_kelembagaan ON kelembagaan_kota.bentuk_kelembagaan_id = bentuk_kelembagaan.id")

		baseQ = baseQ.Where("UPPER(nomenklatur) LIKE ?", "%"+strings.ToUpper(q)+"%").Or(
			"UPPER(alamat) LIKE ?", "%"+strings.ToUpper(q)+"%").Or(
			"UPPER(nama_pimpinan) LIKE ?", "%"+strings.ToUpper(q)+"%").Or(
			"UPPER(no_telp) LIKE ?", "%"+strings.ToUpper(q)+"%").Or(
			"UPPER(email) LIKE ?", "%"+strings.ToUpper(q)+"%").Or(
			"UPPER(perda_pembentukan) LIKE ?", "%"+strings.ToUpper(q)+"%").Or(
			"UPPER(struktur_organisasi) LIKE ?", "%"+strings.ToUpper(q)+"%").Or(
			"UPPER(nama_personil) LIKE ?", "%"+strings.ToUpper(q)+"%").Or(
			"UPPER(keterangan) LIKE ?", "%"+strings.ToUpper(q)+"%").Or(
			"UPPER(kabkota.name) LIKE ?", "%"+strings.ToUpper(q)+"%").Or(
			"UPPER(bentuk_kelembagaan.name) LIKE ?", "%"+strings.ToUpper(q)+"%")
	}

	if sortStr != "" {
		// Do join kabkota for sorting
		baseQ = baseQ.Joins("LEFT JOIN kabkota ON kelembagaan_kota.kabkota_id = kabkota.id")
		// Do join bentuk kelembagaan for sorting
		baseQ = baseQ.Joins("LEFT JOIN bentuk_kelembagaan ON kelembagaan_kota.bentuk_kelembagaan_id = bentuk_kelembagaan.id")

		sortParsed, err := url.PathUnescape(sortStr)
		if err != nil {
			return err
		}
		var s []reqresp.SortRequest
		err = json.Unmarshal([]byte(sortParsed), &s)
		if err != nil {
			return err
		}
		if s[0].Desc {
			baseQ = baseQ.Order(s[0].Selector + " desc")
		} else {
			baseQ = baseQ.Order(s[0].Selector)
		}
	}

	//execute query
	baseQ.Offset(offset).Limit(limit).Find(&records)

	baseQ.Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       records,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

// CreateKelembagaanKota Create a KelembagaanKota
// @Summary Create a KelembagaanKota
// @Description Create a KelembagaanKota
// @Tags Statistik Kelembagaan
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param data body models.KelembagaanKota true "KelembagaanKota data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /kelembagaan-kota [post]
func CreateKelembagaanKota(c *fiber.Ctx) error {
	var record models.KelembagaanKota

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	result := database.DB.Preload(clause.Associations).Create(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "kelembagaan_kota", "insert", nil, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success creating record",
		Data:    record,
	})
}

// GetKelembagaanKota Get single KelembagaanKota data
// @Summary Get single KelembagaanKota data
// @Description Get single KelembagaanKota data
// @Tags Statistik Kelembagaan
// @Accept json
// @Produce json
// @param id path int true "KelembagaanKota ID"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 404 {object} reqresp.ErrorResponse
// @Router /kelembagaan-kota/{id} [get]
func GetKelembagaanKota(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.KelembagaanKota{Id: uint(id)}

	result := database.DB.Preload(clause.Associations).Find(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success getting a record",
		Data:    record,
	})
}

// UpdateKelembagaanKota Update a KelembagaanKota
// @Summary Update a KelembagaanKota
// @Description Update a KelembagaanKota
// @Tags Statistik Kelembagaan
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "KelembagaanKota ID"
// @param data body models.KelembagaanKota true "KelembagaanKota data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /kelembagaan-kota/{id} [put]
func UpdateKelembagaanKota(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.KelembagaanKota{Id: uint(id)}

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	var oldData models.KelembagaanKota
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Model(&record).Preload(clause.Associations).Updates(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to update record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "kelembagaan_kota", "update", &oldData, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating record",
		Data:    record,
	})
}

// DeleteKelembagaanKota Delete a KelembagaanKota
// @Summary Delete a KelembagaanKota
// @Description Delete a KelembagaanKota
// @Tags Statistik Kelembagaan
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "KelembagaanKota ID"
// @Success 204 {object} reqresp.SuccessResponse
// @Error 500 {object} reqresp.ErrorResponse
// @Router /kelembagaan-kota/{id} [delete]
func DeleteKelembagaanKota(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.KelembagaanKota{Id: uint(id)}

	var oldData models.KelembagaanKota
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Delete(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to delete record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "kelembagaan_kota", "delete", &oldData, nil)

	c.Status(fiber.StatusNoContent)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "KelembagaanKota record deleted",
		Data:    record,
	})
}

// GetStatsKelembagaanKota Statistik KelembagaanKota
// @Summary Statistik KelembagaanKota
// @Description Statistik KelembagaanKota
// @Tags Statistik Kelembagaan
// @Accept json
// @Produce json
// @param provinsi_id query int false "Provinsi ID"
// @param kabkota_id query int false "Kab/Kota ID"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.SuccessResponse
// @Router /kelembagaan-kota/stats [get]
func GetStatsKelembagaanKota(c *fiber.Ctx) error {
	kabkotaId, _ := strconv.Atoi(c.Query("kabkota_id", "0"))
	provinsiId, _ := strconv.Atoi(c.Query("provinsi_id", "0"))

	var records []reqresp.StatsResponse

	baseQ := database.DB.Table("bispro.kelembagaan_kota k").Select("bk.name, count(*) as total")
	if kabkotaId > 0 {
		baseQ = baseQ.Where("k.kabkota_id = ?", kabkotaId)
	}
	if provinsiId > 0 {
		baseQ = baseQ.Where("k.kabkota_id IN (select id from master_ref.kabkota WHERE provinsi_id = ?)", provinsiId)
	}
	baseQ = baseQ.Joins("join master_ref.bentuk_kelembagaan bk on bk.id = k.bentuk_kelembagaan_id").Group("bk.name")

	//execute query
	baseQ.Find(&records)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success calculating KelembagaanKota statistic",
		Data:    records,
	})
}
