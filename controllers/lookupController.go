package controllers

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"math"
	"net/url"
	"sidat-amws/database"
	"sidat-amws/models"
	"sidat-amws/models/reqresp"
	"sidat-amws/util"
	"strconv"
	"strings"
)

// AllLookup Lookup list
// @Summary Lookup list
// @Description Lookup list
// @Tags Master
// @Accept json
// @Produce json
// @param group_name query string false "Group name"
// @param name query string false "Name"
// @param code query string false "Code"
// @param sort query string false "Sort by"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /master/lookups [get]
func AllLookup(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	var lookups []models.Lookup

	groupName := c.Query("group_name", "")
	name := c.Query("name", "")
	code := c.Query("code", "")
	sortStr := c.Query("sort", "")

	baseQ :=database.DB
	if groupName != "" {
		baseQ = baseQ.Where("group_name = ?", groupName)
	}
	if name != "" {
		baseQ = baseQ.Where("UPPER(name) LIKE ?", "%"+strings.ToUpper(name)+"%")
	}
	if code != "" {
		baseQ = baseQ.Where("UPPER(code) LIKE ?", "%"+strings.ToUpper(code)+"%")
	}

	if sortStr != "" {
		sortParsed, err := url.PathUnescape(sortStr)
		if err != nil {
			return err
		}
		var s []reqresp.SortRequest
		err = json.Unmarshal([]byte(sortParsed), &s)
		if err != nil {
			return err
		}
		if s[0].Desc {
			baseQ = baseQ.Order(s[0].Selector + " desc")
		} else {
			baseQ = baseQ.Order(s[0].Selector)
		}
	}

	baseQ.Offset(offset).Limit(limit).Find(&lookups)

	baseQ.Model(&models.Lookup{}).Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       lookups,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

// CreateLookup Create a lookup
// @Summary Create a lookup
// @Description Create a lookup
// @Tags Master
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param lookup body models.Lookup true "Lookup data"
// @Success 200 {object} models.Lookup
// @Error 400 {object} reqresp.ErrorResponse
// @Router /master/lookups [post]
func CreateLookup(c *fiber.Ctx) error {
	var lookup models.Lookup

	if err := c.BodyParser(&lookup); err != nil {
		return err
	}

	result := database.DB.Create(&lookup)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create lookup: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "lookups", "insert", nil, &lookup)

	return c.JSON(lookup)
}

// GetLookup Get single lookup data
// @Summary Get single lookup data
// @Description Get single lookup data
// @Tags Master
// @Accept json
// @Produce json
// @param id path int true "Lookup ID"
// @Success 200 {object} models.Lookup
// @Error 404 {object} reqresp.ErrorResponse
// @Router /master/lookups/{id} [get]
func GetLookup(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	lookup := models.Lookup{Id: uint(id)}

	result := database.DB.Find(&lookup)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(lookup)
}

// GetLookupByCode Get single lookup data by Code
// @Summary Get single lookup data by Code
// @Description Get single lookup data by Code
// @Tags Master
// @Accept json
// @Produce json
// @param code query int true "Lookup Code"
// @Success 200 {object} models.Lookup
// @Error 404 {object} reqresp.ErrorResponse
// @Router /master/lookups/by-code [get]
func GetLookupByCode(c *fiber.Ctx) error {
	code := c.Params("code")

	lookup := models.Lookup{Code: code}

	result := database.DB.Where("code = ?", code).Find(&lookup)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(lookup)
}

// UpdateLookup Update a lookup
// @Summary Update a lookup
// @Description Update a lookup
// @Tags Master
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Lookup ID"
// @param lookup body models.Lookup true "Lookup data"
// @Success 200 {object} models.Lookup
// @Error 400 {object} reqresp.ErrorResponse
// @Router /master/lookups/{id} [put]
func UpdateLookup(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	lookup := models.Lookup{Id: uint(id)}

	var oldData models.Lookup
	database.DB.Where("id = ?", id).First(&oldData)

	if err := c.BodyParser(&lookup); err != nil {
		return err
	}

	result := database.DB.Model(&lookup).Updates(&lookup)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to update record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "lookups", "update", &oldData, &lookup)

	return c.JSON(lookup)
}

// DeleteLookup Delete a lookup
// @Summary Delete a lookup
// @Description Delete a lookup
// @Tags Master
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "Lookup ID"
// @Success 204 {object} reqresp.SuccessResponse
// @Error 500 {object} reqresp.ErrorResponse
// @Router /master/lookups/{id} [delete]
func DeleteLookup(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	lookup := models.Lookup{Id: uint(id)}

	var oldData models.Lookup
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Delete(&lookup)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to delete record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "lookups", "delete", &oldData, nil)

	c.Status(fiber.StatusNoContent)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Lookup record deleted",
		Data:    lookup,
	})
}

type LookupGroups struct {
	Id        uint
	GroupName string
}

// AllGroupName Lookup Group list
// @Summary Lookup Group list
// @Description Lookup Group list
// @Tags Master
// @Accept json
// @Produce json
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.SuccessResponse
// @Router /master/lookups/group-name [get]
func AllGroupName(c *fiber.Ctx) error {
	var lookupGroups []LookupGroups

	database.DB.Distinct("id", "group_name").Order("group_name").Find(&lookupGroups)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Successfully getting records",
		Data:    lookupGroups,
	})
}

// SearchGroupName Search group name
// @Summary Search group name
// @Description Search group name
// @Tags Master
// @Accept json
// @Produce json
// @param q query string true "Search string"
// @Success 200 {object} reqresp.SuccessResponse
// @Router /master/lookups/search-group-name [get]
func SearchGroupName(c *fiber.Ctx) error {
	q := c.Query("q")

	var lookupGroups []LookupGroups

	database.DB.Distinct("id", "group_name").Where("group_name LIKE ?", "%"+q+"%").Order("group_name").Find(&lookupGroups)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Successfully getting records",
		Data:    lookupGroups,
	})
}

// AllNameByGroupName Get all lookup by group name
// @Summary Get all lookup by group name
// @Description Get all lookup by group name
// @Tags Master
// @Accept json
// @Produce json
// @param group_name query string true "Group name"
// @Success 200 {object} reqresp.SuccessResponse
// @Router /master/lookups/by-group-name [get]
func AllNameByGroupName(c *fiber.Ctx) error {
	groupName := c.Query("group_name")

	var lookups []models.Lookup

	database.DB.Where("group_name = ?", groupName).Order("name").Find(&lookups)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Successfully getting records",
		Data:    lookups,
	})
}
