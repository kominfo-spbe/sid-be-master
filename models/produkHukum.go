package models

import (
	"gorm.io/datatypes"
	"time"
)

type ProdukHukum struct {
	Id                 uint                  `json:"id" gorm:"primaryKey"`
	CreatedAt          time.Time             `json:"created_at"`
	UpdatedAt          time.Time             `json:"updated_at"`
	DirektoratId       uint                  `json:"direktorat_id"`
	Direktorat         Direktorat            `json:"direktorat" gorm:"foreignKey:DirektoratId;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	SubdirektoratId    uint                  `json:"subdirektorat_id"`
	Subdirektorat      Subdirektorat         `json:"subdirektorat" gorm:"foreignKey:SubdirektoratId;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	JenisPerundanganId uint                  `json:"jenis_perundangan_id"`
	JenisPerundangan   JenisPerundangan      `json:"jenis_perundangan" gorm:"foreignKey:JenisPerundanganId;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	Nama               string                `json:"nama"`
	Tahun              int                   `json:"tahun"`
	Target             datatypes.Date        `json:"target"`
	Keterangan         string                `json:"keterangan"`
	AddNote            string                `json:"add_note"`
	Progress           []ProdukHukumProgress `json:"progress"`
}

func (p ProdukHukum) TableName() string {
	return "bispro.produk_hukum"
}
