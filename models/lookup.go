package models

import (
	"time"
)

type Lookup struct {
	Id        uint           `json:"id" gorm:"primaryKey"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	Code      string         `json:"code"`
	Name      string         `json:"name"`
	GroupName string         `json:"group_name"`
}

func (l Lookup) TableName() string {
	return "master_ref.lookup"
}
