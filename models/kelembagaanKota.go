package models

import (
	"time"
)

type KelembagaanKota struct {
	Id                  uint              `json:"id" gorm:"primaryKey"`
	CreatedAt           time.Time         `json:"created_at"`
	UpdatedAt           time.Time         `json:"updated_at"`
	KabkotaId           uint              `json:"kabkota_id"`
	Kabkota             Kabkota           `json:"kabkota" gorm:"foreignKey:KabkotaId;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
	BentukKelembagaanId uint              `json:"bentuk_kelembagaan_id"`
	BentukKelembagaan   BentukKelembagaan `json:"bentuk_kelembagaan" gorm:"foreignKey:BentukKelembagaanId;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	Nomenklatur         string            `json:"nomenklatur"`
	Alamat              string            `json:"alamat"`
	NamaPimpinan           string            `json:"nama_pimpinan"`
	NoTelp                 string            `json:"no_telp"`
	Email                  string            `json:"email"`
	PerdaPembentukan       string            `json:"perda_pembentukan"`
	PerdaPembentukanPath   string            `json:"perda_pembentukan_path"`
	StrukturOrganisasi     string            `json:"struktur_organisasi"`
	StrukturOrganisasiPath string            `json:"struktur_organisasi_path"`
	NamaPersonil           string            `json:"nama_personil"`
	NamaPersonilPath       string            `json:"nama_personil_path"`
	Keterangan             string            `json:"keterangan"`
}

func (k KelembagaanKota) TableName() string {
	return "bispro.kelembagaan_kota"
}
